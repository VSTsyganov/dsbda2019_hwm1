package ru.mephi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataReducerTest {
    private ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        DataReducer reducer = new DataReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testReducer_executesSuccessfully_withSingleInputRow() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        reduceDriver.withInput(new Text("216"), values)
                    .withOutput(new Text("216"), new IntWritable(2))
                    .runTest();
    }

    @Test
    public void testReducer_executesSuccessfully_withSeveralInputRows() throws IOException {
        List<IntWritable> values1 = new ArrayList<IntWritable>();
        values1.add(new IntWritable(1));
        values1.add(new IntWritable(1));
        List<IntWritable> values2 = new ArrayList<IntWritable>();
        values2.add(new IntWritable(1));
        List<IntWritable> values3 = new ArrayList<IntWritable>();
        values3.add(new IntWritable(1));
        reduceDriver.withInput(new Text("216"), values1)
                    .withInput(new Text("226"), values2)
                    .withInput(new Text("233"), values3)
                    .withOutput(new Text("216"), new IntWritable(2))
                    .withOutput(new Text("226"), new IntWritable(1))
                    .withOutput(new Text("233"), new IntWritable(1))
                    .runTest();
    }
}
