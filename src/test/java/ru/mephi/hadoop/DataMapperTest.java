package ru.mephi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class DataMapperTest {
    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;

    @Before
    public void setUp() {
        DataMapper mapper = new DataMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapper_executesSuccessfully_withSingleInputRow() throws IOException {
        mapDriver.withInput(new LongWritable(1), new Text("dbe06f7b386d619d3ab4dde3c93ed6d3\t20131020015400898\t1\tC6G2Ng4C2m\tOpera/9.80 (Windows NT 5.1; Edition IBIS) Presto/2.12.388 Version/12.10\t14.126.118.*\t216\t216\t2\tbe2441d0219febc9149a807c39bde186\ta763717a9f8d613708311ed70940fa7a\tnull\t2759127038\t200\t200\tOtherView\tNa\t5\t7319\t277\t37\tnull\t2259\t10057,10059,14273,13866,10006,10110,10031,10052,10127,10063,10116"))
                 .withOutput(new Text(String.valueOf(216)), new IntWritable(1))
                 .runTest();
    }

    @Test
    public void testMapper_executesSuccessfully_withSeveralInputRows() throws IOException {
        // Line 3 with city code 217 won't be in map output as it has only 21 columns instead of 24 after split
        mapDriver.withInput(new LongWritable(1), new Text("dbe06f7b386d619d3ab4dde3c93ed6d3\t20131020015400898\t1\tC6G2Ng4C2m\tOpera/9.80 (Windows NT 5.1; Edition IBIS) Presto/2.12.388 Version/12.10\t14.126.118.*\t216\t216\t2\tbe2441d0219febc9149a807c39bde186\ta763717a9f8d613708311ed70940fa7a\tnull\t2759127038\t200\t200\tOtherView\tNa\t5\t7319\t277\t37\tnull\t2259\t10057,10059,14273,13866,10006,10110,10031,10052,10127,10063,10116"))
                 .withInput(new LongWritable(2), new Text("9d41c92515ba05d44fe6d9aa5a28fd69\t20131020023901629\t1\tC951ZD798mR\tMozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE 2.X MetaSr 1.0\t113.85.207.*\t216\t226\t1\t20fc675468712705dbf5d3eda94126da\taaab99df3a0114eaf5728a76a2bd111\tnull\tmm_10982364_973726_8930541\t300\t250\tFourthView\tNa\t0\t7323\t294\t47\tnull\t2259\tnull"))
                 .withInput(new LongWritable(3), new Text("c5e7563166c1f5477478371d937e9eb0\t20131020103102081\t1\tCAMEY6D4cBs\tMozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)\t119.131.155.*\t216\t217\t2\t8f69cfddb6d1c89f73104595a09394aa\t6944dcbb6727abc5cc36e8476a313798\tnull\t1621520185\t728\t90\tOtherView\tNa\t62\t7330\t277\t1"))
                 .withInput(new LongWritable(4), new Text("dd5c2a9324de82aa07af8ce0cf4e348\t20131020043201565\t1\tCANGkv9Beql\tMozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; QQBrowser/7.4.14018.400)\t121.12.0.*\t216\t216\t3\tdd4270481b753dde29898e27c7c03920\td2bdfdce12b2a3545cd25628d87c7f3b\tnull\tEnt_F_Width1\t1000\t90\tNa\tNa\t70\t7336\t294\t70\tnull\t2259\t10057,10059,10077,10075,10083,10006,10111,10126,10131,13403,10063,10116"))
                 .withInput(new LongWritable(5), new Text("709b7cffadaefd399738439386ece34d\t20131020104600653\t1\tCAOI72FHgYj\tMozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)\t123.88.187.*\t216\t233\t2\tabc5a8aaa90bafdd7d6b60683e6f7ed3\ta6e53cda7ceff62ea2dac7f281446fa5\tnull\t4254532549\t300\t250\tFirstView\tNa\t5\t7323\t277\t84\tnull\t2259\t11278,13800,10684,13042,10006,10110,10123,13776,10031,10063"))
                 .withOutput(new Text("216"), new IntWritable(1))
                 .withOutput(new Text("226"), new IntWritable(1))
                 .withOutput(new Text("216"), new IntWritable(1))
                 .withOutput(new Text("233"), new IntWritable(1))
                 .runTest();
    }
}
