package ru.mephi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class DataPartitioner extends Partitioner<Text, IntWritable> {
    @Override
    public int getPartition(Text text, IntWritable intWritable, int numReduceTasks) {
        if(numReduceTasks < 2)
            return 0;
        else {
            if (text.hashCode() % 2 == 0)
                return 0;
            else return 1;
        }
    }
}
