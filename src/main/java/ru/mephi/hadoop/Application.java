package ru.mephi.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
//import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        System.out.println("Java code: started main");
        if(args.length < 4) {
            System.out.println("Usage: input_dir_name output_dir_name cities_file number_of_reducers");
            return;
        }

        System.out.println("Java code: args:");
        for (int i = 0; i < args.length; i++)
            System.out.println("args[" + i + "] = " + args[i]);

        int numReducers = Integer.parseInt(args[3]);

        Configuration conf = new Configuration();

	conf.set("mapred.textoutputformat.separator", ",");

        Job job = Job.getInstance(conf, "HighBidPricedEventsByCities");

        job.setJarByClass(Application.class);
        job.setMapperClass(DataMapper.class);
        job.setPartitionerClass(DataPartitioner.class);
        job.setReducerClass(DataReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        Path outputPath = new Path(args[1]);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // Add the cache file URL which we passed as the third argument to the program
        DistributedCache.addCacheFile(new Path(args[2]).toUri(), job.getConfiguration());

        outputPath.getFileSystem(conf).delete(outputPath, true);

        // Number of reducers
        job.setNumReduceTasks(numReducers);
        System.out.println("Java code: Number of reducers: " + job.getNumReduceTasks());

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
