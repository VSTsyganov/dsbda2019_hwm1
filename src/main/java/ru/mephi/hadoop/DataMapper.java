package ru.mephi.hadoop;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    // Map of cityId, cityName
    private Map<String, String> cities = new HashMap<String, String>();
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Path[] distributedCacheFiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());
        if(distributedCacheFiles != null && distributedCacheFiles.length > 0) {
            for (Path distributedCacheFile : distributedCacheFiles) {
                readFile(distributedCacheFile);
            }
        }
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String parts[] = line.split("\t");

        if(parts.length == 24) {
            int biddingPrice = Integer.parseInt(parts[19]);
            String cityId = parts[7];
            if(biddingPrice > 250) {
                String cityName = cities.get(cityId);

                // if city name was found by code, it is replaced, otherwise code is preserved
                if(cityName == null)
                    word.set(cityId);
                else word.set(cityName);
                context.write(word, one);
            }
        }
    }

    private void readFile(Path filePath) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(filePath.toString()));
        String line;
        String[] splitLine;
        for(int i = 0; (line = bufferedReader.readLine()) != null; i++) {
            if(i == 0)
                splitLine = line.split(" ");
            else
                splitLine = line.split("\t");

            if (splitLine.length != 2)
                continue;
            String cityId = splitLine[0];
            cities.put(cityId, splitLine[1]);
        }
    }
}
