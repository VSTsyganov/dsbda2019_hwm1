# Homework 1

DS:BDA Homework 1 repo

### Prerequisites
- 64-bit VMs require a 64-bit host OS and a virtualization product that can support a 64-bit guest OS.
- The amount of RAM required by the VM: 8+ GiB 

### MANUAL

Build project: 
```sh
$ git clone git clone https://VSTsyganov@bitbucket.org/VSTsyganov/dsbda2019_hmw1.git
$ cd ./dsbda2019_hwm1
```
Prepare data:
```sh
$ chmod +x ./start.sh
```
Run:
```sh
$ ./start.sh
```
Show output in HDFS:
```sh
$ hadoop fs -text /output/*
```


More information in [here](Report_hw1.docx)
