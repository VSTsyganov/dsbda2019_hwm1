#!/bin/bash

# Preparing directories and files
hdfs dfs -rm -R /input /cities /output

hdfs dfs -mkdir /input

hdfs dfs -mkdir /cities

hdfs dfs -mkdir /output

rm -f -r ./output/*

hadoop dfs -put ./input/* /input

hadoop dfs -put ./cities/cities.en.txt /cities

hadoop fs -ls /cities

hadoop fs -ls /input

# Precode
time mvn test

time mvn package

cd ./target/

# Start 
hadoop jar dsbda2019.tsyganov-1.0-SNAPSHOT.jar ru.mephi.hadoop.Application /input /output /cities/cities.en.txt 4

# Extract and display files
hadoop fs -get /output/* ../output/

for i in 0 1 2 3
do
cat ../output/part-r-0000$i > ../output/csv-0000$i.csv
done

echo "First csv file"
echo "---------------------------------------------------------------"
cat ../output/csv-00000.csv
echo "---------------------------------------------------------------"
echo "Second csv file"
echo "---------------------------------------------------------------"
cat ../output/csv-00001.csv
echo "---------------------------------------------------------------"
